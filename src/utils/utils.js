import { Dimensions, Platform, PixelRatio } from 'react-native';

export const baseWith = 320;
export const baseHeight = 568;
export const screen = Dimensions.get('screen');
export const primaryColor = '#FFFFFF';
export const secondColor = '#2d80b3';
export const bgInputColor = '#f2f2f4';
export const inactiveTintColor = '#605F60';
export const bgLoadingColor = '#F6F7F9';
export const activeTintColor = '#FF0000';
export const hsb2028351 = '#165a81';
export const hsb1948475 = '#1e9ac0';
export const hsb0085 = '#d8d8d8';
export const textColor = '#333333';
export const placeholderColor = '#8f8e94';
export const pixelSize = 1 / PixelRatio.getPixelSizeForLayoutSize(1);
export const apiGoogleMapKey = 'AIzaSyAALvKycxKo-PoQNX_A2nxl4hvYmdkSgnM';
export const latitudeDelta = 0.01986
export const longitudeDelta = latitudeDelta * screen.width / screen.height;

export const statusBarHeight = Platform.select({ ios: 20, android: 24 });

export const textStyle = {
  fontFamily: 'SF UI Display',
  fontSize: Scale(12),
  fontWeight: Platform.select({ ios: '300', android: '100' }),
  color: textColor,
};

export function Scale(size = 12) {
  const scaleWith = screen.width / baseWith;
  const scaleHeight = screen.height / baseHeight;
  const scale = Math.min(scaleWith, scaleHeight);
  return Math.ceil(scale * (size + Platform.select({ ios: 1, android: 0 })));
}

export const navBarHeight = Scale(Platform.select({ ios: 35, android: 25 }));
export const tabBarHeight = Scale(35);
