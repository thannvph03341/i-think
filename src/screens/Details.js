import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
// import RNVideoController from 'react-native-video-controls';
// import { TextTrackType } from 'react-native-video';
import RNVideo from 'react-native-af-video-player';
import { screen } from '../utils/utils';


class Details extends Component {
  render() {
    return (<View style={Styles.root}>
      <View style={Styles.viewVideo}>
        {/* <RNVideo 
        textTracks={}/> */}
        <RNVideo 
          autoPlay
          logo='https://upload.wikimedia.org/wikipedia/en/thumb/e/e0/WPVG_icon_2016.svg/1024px-WPVG_icon_2016.svg.png'
          url={`https://r4---sn-npoe7ner.googlevideo.com/videoplayback?id=o-AJefRGC-Zm9QjHLHt-im_WhLrevJBiW5vVsIleLeFhux&itag=22&source=youtube&requiressl=yes&pl=24&ei=e-70XK64FIbZhwbV74-oAQ&mime=video%2Fmp4&ratebypass=yes&dur=180.093&lmt=1472029623861086&fvip=6&c=WEB&ip=172.241.233.117&ipbits=0&expire=1559577307&sparams=dur,ei,expire,id,ip,ipbits,itag,lmt,mime,mip,mm,mn,ms,mv,pl,ratebypass,requiressl,source&key=cms1&signature=1FDAEB5E3452C69C6496E042D02772E9F4A2A174.143C7BEEB9B353BB48E39CD8A64041F6698396FD&video_id=XXPlZJan--s&title=Video+c%E1%BB%B1c+%C4%91%E1%BA%B9p+%C4%91%E1%BB%83+test+m%C3%A0n+h%C3%ACnh+full+HD&redirect_counter=1&cm2rm=sn-ab5ees7z&req_id=8ffa7dad0c22a3ee&cms_redirect=yes&mip=118.70.116.157&mm=34&mn=sn-npoe7ner&ms=ltu&mt=1559555620&mv=m`}
        />
        {/* <RNVideo
          source={{ uri: 'https://r4---sn-npoe7ner.googlevideo.com/videoplayback?id=o-AJefRGC-Zm9QjHLHt-im_WhLrevJBiW5vVsIleLeFhux&itag=22&source=youtube&requiressl=yes&pl=24&ei=e-70XK64FIbZhwbV74-oAQ&mime=video%2Fmp4&ratebypass=yes&dur=180.093&lmt=1472029623861086&fvip=6&c=WEB&ip=172.241.233.117&ipbits=0&expire=1559577307&sparams=dur,ei,expire,id,ip,ipbits,itag,lmt,mime,mip,mm,mn,ms,mv,pl,ratebypass,requiressl,source&key=cms1&signature=1FDAEB5E3452C69C6496E042D02772E9F4A2A174.143C7BEEB9B353BB48E39CD8A64041F6698396FD&video_id=XXPlZJan--s&title=Video+c%E1%BB%B1c+%C4%91%E1%BA%B9p+%C4%91%E1%BB%83+test+m%C3%A0n+h%C3%ACnh+full+HD&redirect_counter=1&cm2rm=sn-ab5ees7z&req_id=8ffa7dad0c22a3ee&cms_redirect=yes&mip=118.70.116.157&mm=34&mn=sn-npoe7ner&ms=ltu&mt=1559555620&mv=m' }}
          navigator={this.props.navigation}
          posterResizeMode='contain'
          toggleResizeModeOnFullscreen={false}
          paused={false}
          style={Styles.viewVideo}
          selectedTextTrack={{
            type: 'index',
            value: "English CC"
          }}
          textTracks={[
            {
              title: "English CC",
              language: "en",
              type: TextTrackType.VTT, // "text/vtt"
              uri: "https://bitdash-a.akamaihd.net/content/sintel/subtitles/subtitles_en.vtt"
            },
            {
              title: "Spanish Subtitles",
              language: "es",
              type: TextTrackType.SRT, // "application/x-subrip"
              uri: "https://durian.blender.org/wp-content/content/subtitles/sintel_es.srt"
            }
          ]}/> */}
      </View>
      <View>

      </View>
    </View>)
  }
}

const Styles = StyleSheet.create({
  root: {
    flex: 1
  },
  viewVideo: {
    width: screen.width,
    height: screen.height / 3
  },
  detail: {
    flex: 1,
  },
});

export default Details;