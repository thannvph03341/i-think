// https://medium.com/@sxia/how-to-customize-tab-bar-in-react-navigation-a0dc6d4d7e61

import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import { BottomTabBarProps } from 'react-navigation';
import {
  Scale,
  pixelSize,
  borderColor,
  secondColor,
  activeTintColor,
  inactiveTintColor,
  tabBarHeight
} from '../utils/utils';
import Icons from 'react-native-vector-icons/MaterialCommunityIcons';
import I18n from '../I18n';
import Text from './Text';
import { get } from 'lodash';

const OMenu = {
  Home: {
    title: I18n.t('tabBar.home'),
    icon: 'owl'
  },
  Trending: {
    title: I18n.t('tabBar.trending'),
    icon: 'cube-outline'
  },
  Inbox: {
    title: I18n.t('tabBar.inbox'),
    icon: 'biohazard'
  },
  Library: {
    title: I18n.t('tabBar.library'),
    icon: 'cannabis'
  }
}

class TabBar extends Component<BottomTabBarProps> {

  constructor(props) {
    super(props)
    this.state = {
      currentIndex: 0
    }
    this.onTabPress = this.onTabPress.bind(this);
    this.renderTabBottom = this.renderTabBottom.bind(this);
  }

  onTabPress(routeName, index) {
    const { navigation, } = this.props;
    if (index !== navigation.state.index) {
      navigation.navigate(routeName);
    }
  }

  renderTabBottom(item, index) {
    const { navigation, activeTintColor, inactiveTintColor } = this.props;
    const color = index === navigation.state.index ? activeTintColor : inactiveTintColor;
    return <TouchableOpacity
      onPress={() => { this.onTabPress(item.routeName, index); }}
      style={styles.item}
      key={item.key}>
      <Icons style={[styles.icon, { color }]} name={get(OMenu[item.key], 'icon')} />
      <Text style={[styles.labelItem, { color }]}>{get(OMenu[item.key], 'title')}</Text>
    </TouchableOpacity>
  }

  render() {
    const routers = get(this.props, 'navigation.state.routes', []);
    return (
      <View style={[styles.container]}>
        {routers.map(this.renderTabBottom)}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    height: tabBarHeight,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderTopWidth: pixelSize,
    borderTopColor: borderColor,
    paddingTop: 3
  },
  item: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  labelItem: {
    fontSize: Scale(8),
    color: inactiveTintColor
  },
  icon: {
    fontSize: Scale(16),
    color: inactiveTintColor,
  }
});

export default TabBar;