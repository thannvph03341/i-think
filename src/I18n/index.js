import I18n from 'react-native-i18n';
import vn from './locales/vn';
import en from './locales/en';

I18n.fallbacks = true;
I18n.locale = 'vn';
I18n.translations = { vn, en };

export default I18n;