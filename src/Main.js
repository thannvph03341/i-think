import React, { Component, lazy } from 'react';
import {
  createBottomTabNavigator,
  SafeAreaView,
  createAppContainer,
  createStackNavigator
} from 'react-navigation';
import Home from './screens/Home';
import Trending from './screens/Trending';
import Inbox from './screens/inbox';
import Library from './screens/Library';
import TabBar from './components/TabBar';
import InjectMain from './HOCs/injectMain';
import Detail from './screens/Details';

import { activeTintColor, inactiveTintColor } from './utils/utils';


const TabNavigation = createBottomTabNavigator({
  Home: {
    screen: Home,
  },
  Trending: {
    screen: Trending
  },
  Inbox: {
    screen: Inbox
  },
  Library: {
    screen: Library
  },
}, {
    initialRouteName: 'Home',
    lazy: false,
    tabBarComponent: (props) => <TabBar {...props} />,
    tabBarOptions: {
      activeTintColor,
      inactiveTintColor
    },
    initialRouteParams: {},
    animationEnabled: true,
  })


const stackNavigation = createStackNavigator({
  Root: TabNavigation,
  Detail: {
    screen: Detail
  }
}, { 
  initialRouteName: 'Root',
  headerMode: 'none'
 })

export default InjectMain(createAppContainer(stackNavigation));