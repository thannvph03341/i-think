// https://medium.com/appandflow/react-native-collapsible-navbar-e51a049b560a
import React, { Component } from 'react';
import { View, Animated, StyleSheet, TouchableOpacity } from 'react-native';
import { navBarHeight, statusBarHeight, Scale, } from '../utils/utils';
import Text from './Text';
import I18n from '../I18n';
import Icons from 'react-native-vector-icons/MaterialCommunityIcons';

const scrollAnim = new Animated.Value(0);
const offsetAnim = new Animated.Value(0);
class Container extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      scrollAnim,
      offsetAnim,
      clampedScroll: Animated.diffClamp(
        Animated.add(
          scrollAnim.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 1],
            extrapolateLeft: 'clamp'
          }),
          offsetAnim
        ),
        0,
        navBarHeight - statusBarHeight
      )
    };
  }

  _clampedScrollValue = 0;
  _offsetValue = 0;
  _scrollValue = 0;

  componentDidMount() {
    this.state.scrollAnim.addListener(({ value }) => {
      const diff = value - this._scrollValue;
      this._scrollValue = value;
      this._clampedScrollValue = Math.min(
        Math.max(this._clampedScrollValue + diff, 0),
        navBarHeight - statusBarHeight
      );
    });
    this.state.offsetAnim.addListener(({ value }) => {
      this._offsetValue = value;
    });
  }

  componentWillUnmount() {
    this.state.scrollAnim.removeAllListeners();
    this.state.offsetAnim.removeAllListeners();
  }

  _onMomentumScrollBegin = () => {
    clearTimeout(this._scrollEndTimer);
  }

  _onMomentumScrollEnd = () => {
    const toValue = this._scrollValue > navBarHeight &&
      this._clampedScrollValue > (navBarHeight - statusBarHeight) / 2
      ? this._offsetValue + navBarHeight
      : this._offsetValue - navBarHeight
    Animated.timing(this.state.offsetAnim, {
      toValue,
      duration: 350,
      useNativeDriver: true
    }).start();
  }

  _onScrollEndDrag = () => {
    this._scrollEndTimer = setTimeout(this._onMomentumScrollEnd, 250);
  }

  render() {
    const { clampedScroll } = this.state;
    const navbarTranslate = clampedScroll.interpolate({
      inputRange: [0, navBarHeight - statusBarHeight],
      outputRange: [0, -(navBarHeight + statusBarHeight)],
      extrapolate: 'clamp'
    });
    const navBarOpacity = clampedScroll.interpolate({
      inputRange: [0, navBarHeight - statusBarHeight],
      outputRange: [1, 0],
      extrapolate: 'clamp'
    });
    return (
      <View>
        <Animated.ScrollView
          style={{ flexGrow: 1, }}
          contentContainerStyle={{ paddingTop: navBarHeight }}
          scrollEventThrottle={1}
          onMomentumScrollBegin={this._onMomentumScrollBegin}
          onMomentumScrollEnd={this._onMomentumScrollEnd}
          onScrollEndDrag={this._onScrollEndDrag}
          onScroll={Animated.event(
            [{ nativeEvent: { contentOffset: { y: this.state.scrollAnim } } }],
            { useNativeDriver: true }
          )}>
          {this.props.children}
        </Animated.ScrollView>
        <Animated.View
          style={[Styles.header, { transform: [{ translateY: navbarTranslate }] }]}>
          <View
            style={[Styles.vLogo]}>
            <Icons
              name='fire'
              style={Styles.logo} />
            <Text style={Styles.tLogo}>{I18n.t('common.logo')}</Text>
          </View>
          <View style={Styles.rView}>
            <TouchableOpacity
              style={Styles.sButton}>
              <Icons
                name='magnify'
                style={Styles.iButton} />
            </TouchableOpacity>
            <TouchableOpacity
              style={Styles.sButton}>
              <Icons
                name='bell-outline'
                style={Styles.iButton} />
            </TouchableOpacity>
          </View>
        </Animated.View>
      </View>
    )
  }
}

const Styles = StyleSheet.create({
  header: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    height: navBarHeight,
    backgroundColor: 'white',
    borderBottomColor: '#dedede',
    borderBottomWidth: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  vLogo: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    margin: 6,
  },
  logo: {
    fontSize: Scale(30),
    color: 'red',
  },
  tLogo: {
    fontSize: Scale(18),
    fontWeight: '900',
    color: 'red'
  },
  rView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 6
  },
  iButton: {
    fontSize: Scale(18)
  },
  sButton: {
    padding: 6,
    alignItems: 'center'
  }
});

export default Container;