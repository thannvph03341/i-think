import React, { Component } from 'react';
import { Text, TextProps } from 'react-native';
import { textColor, textStyle } from '../utils/utils';

export default class cText extends Component<TextProps> {
  render() {
    const { style, ...other } = this.props;
    return (
      <Text style={[textStyle, style]} {...other}>{this.props.children}</Text>
    )
  }
}