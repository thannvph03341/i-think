import React, { Component } from 'react';
import { View, StyleSheet, Animated, Image, TouchableOpacity } from 'react-native';
import {
  screen,
  primaryColor,
  navBarHeight,
  statusBarHeight,
  tabBarHeight,
  pixelSize,
  secondColor,
  bgLoadingColor,
  Scale,
} from '../utils/utils';
import NoPhoto from '../assets/no_photo.png';
import Text from './Text';
import Icons from 'react-native-vector-icons/MaterialCommunityIcons';

const hContent = (screen.height / 2) - (navBarHeight + statusBarHeight + tabBarHeight) / 2;

class ItemListMovie extends Component {

  constructor(props) {
    super(props);
    this.animatedOpacity = new Animated.Value(0);
  }

  componentDidMount() {
    Animated.timing(this.animatedOpacity, {
      toValue: 1,
      duration: 1500,
    }).start();
  }

  _goToDetailScreen = () => {
    this.props.navigation.navigate('Detail')
  }

  render() {
    const opacity = this.animatedOpacity.interpolate({
      inputRange: [0, 1],
      outputRange: [0, 1]
    })

    return (
      <Animated.View
        style={[styles.content, { opacity }]}>
        <TouchableOpacity
        onPress={this._goToDetailScreen}
        style={[styles.cImage]}>
          {/* <Image
            style={styles.img}
            source={{ uri: 'http://aginginhampshire.us/wp-content/uploads/image-form-erkaljonathandedecker-image.jpg' }}
            resizeMode='cover'
          /> */}
          <View style={styles.time}>
            <Text style={styles.tTime}>--:--:--</Text>
          </View>
          <Icons
            name='emoticon-excited-outline'
            style={styles.iconNoContent} />

        </TouchableOpacity>
        <View style={styles.vDetail}>
          <View style={styles.vAccount}>

          </View>
          <View>
            <Text style={[styles.tDetail, styles.tDetailTitle]}></Text>
            <Text style={styles.tDetail}></Text>
            <Text style={[styles.tDetail, { marginBottom: 0 }]}></Text>
          </View>
        </View>
      </Animated.View>
    )
  }
}


const styles = StyleSheet.create({
  content: {
    height: hContent,
    backgroundColor: primaryColor,
    flexDirection: 'column'
  },
  cImage: {
    flexGrow: 1,
    backgroundColor: bgLoadingColor,
    alignItems: 'center',
    justifyContent: 'center'
  },
  img: {
    flexGrow: 1,
    width: screen.width,
  },
  time: {
    position: 'absolute',
    zIndex: 10,
    right: 12,
    bottom: 12,
    backgroundColor: '#332A2B67',
    paddingLeft: 8,
    paddingRight: 8,
    paddingTop: 3,
    paddingBottom: 3,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 6
  },
  tTime: {
    color: 'white'
  },
  vDetail: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 6,
    marginBottom: 6
  },
  vAccount: {
    width: 50,
    height: 50,
    backgroundColor: bgLoadingColor,
    borderRadius: 25,
    margin: 6
  },
  tDetail: {
    width: screen.width - 68,
    marginBottom: 6,
    backgroundColor: bgLoadingColor,
  },
  tDetailTitle: {
    fontSize: Scale(16)
  },
  iconNoContent: {
    fontSize: Scale(66),
    color: '#332A2B67'
  }
});

export default ItemListMovie;