// https://medium.com/appandflow/react-native-collapsible-navbar-e51a049b560a
import React, { Component } from 'react';
import { FlatList } from 'react-native';
import Container from '../components/Container';
import { navBarHeight, statusBarHeight } from '../utils/utils';
import Text from '../components/Text';
import ItemListMovie from '../components/ItemListMovie';

class Home extends Component {
  render() {
    return (
      <Container>
        <FlatList
          data={Array.apply(null, Array(20)).map((_, i) => i)}
          keyExtractor={item => `${item}`}
          renderItem={(item) => <ItemListMovie {...this.props}/>}
        />
      </Container>
    )
  }
}

export default Home;