export default Object.create({
  common: {
    logo: 'IThink',
    loading: 'Loading...'
  },
  tabBar: {
    home: 'Home',
    trending: 'Trending',
    inbox: 'Inbox',
    library: 'Library',
  }
});